
{ // frame
    module cross() {
        rotate([0,-10,0]){
        translate([10,0,0])
            cube([10,5,150]);
        translate([10,0,140])
            cube([240,5,10]);
        }
        translate([0,0,30])
            cube([220,5,10]);
        translate([210,0,0])
            cube([10,5,220]);
        translate([320,0,0])
            cube([10,5,200]);
        translate([210,0,0])
            cube([120,5,10]);
        translate([210,0,0])
            cube([120,5,10]);
        translate([210,0,210])
        rotate([0,10,0])
            cube([120,5,10]);
        }
    for (dy=[0:60:180]) {
        color("tan")
        translate([0,dy,0])
        cross();
        }
}
{ // matress
    color("white")
    translate([10,5,40])
        cube([200,120,20]);
}
{ // roomfloor
    color("maroon")
    translate([10,0,40])
        cube([210,185,1]);
}
{ // floor01
    color("maroon")
    translate([210,0,10])
        cube([120,185,1]);
}
{ // roof
    color("blue",0.2)
    rotate([0,-10,0])
    translate([0,0,150])
        cube([240,185,1]);
    color("grey",1)
    translate([200,0,222])
    rotate([0,10,0])
        cube([150,185,1]);
}
{ // roomwall
    color("tan",0.8)
    rotate([0,-10,0])
    translate([9,0,0])
        cube([1,185,150]);
}
{  // table
    color("maroon")
    translate([210,5,90])
        cube([30,120,3]);
}
{ // door
    color("yellow")
    translate([320,0,11])
    rotate([0,0,60]){
    translate([-10,0,0])
        cube([10,5,180]);
    translate([-100,0,0])
        cube([10,5,190]);
    translate([-100,0,0])
        cube([100,5,10]);
    translate([-100,0,80])
        cube([100,5,10]);
    translate([-100,0,187])
    rotate([0,10,0])
        cube([100,5,10]);
    }
}
{ // 183rule
    *translate([220,-5,50])
    rotate([0,90,0]){
        color("maroon",1)
        cube([5,5,40]);
        color("red",1)
        translate([0,0,40])
            cube([5,5,30]);
        translate([0,0,70])
        color("orange",1)
            cube([5,5,20]);
        color("yellow",1)
        translate([0,0,90])
            cube([5,5,93]);
    }

}
