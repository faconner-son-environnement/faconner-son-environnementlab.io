
{ // frame #1
translate([0,2.7,0])
    cube([2.7,15,150]);
translate([212.3,2.7,0])
    cube([2.7,15,210]);
translate([312.3,2.7,0])
    cube([2.7,15,240]);
translate([0,0,0])
    cube([15,2.7,150]);
        translate([100,0,0])
            cube([15,2.7,180]);
        translate([200,0,0])
            cube([15,2.7,210]);
        translate([300,0,0])
            cube([15,2.7,240]);
        translate([0,2.7,60])
            cube([215,2.7,15]);
        translate([200,2.7,20])
            cube([150,2.7,15]);
        translate([-50,2.7,120])
        rotate([0,-15,0])
            cube([400,2.7,15]);
        }
        
{ // frame #2
    translate([0,100,0]){
        translate([0,-15,0])
            cube([2.7,15,150]);
        translate([212.3,-15,0])
            cube([2.7,15,210]);
        translate([312.3,-15,0])
            cube([2.7,15,230]);
        translate([0,0,0])
            cube([15,2.7,150]);
        translate([100,0,0])
            cube([15,2.7,75]);
        translate([200,0,0])
            cube([15,2.7,210]);
        translate([300,0,0])
            cube([15,2.7,240]);
        translate([0,-2.7,60])
            cube([215,2.7,15]);
        translate([200,-2.7,20])
            cube([115,2.7,15]);
        translate([-50,-2.7,120])
        rotate([0,-15,0])
            cube([400,2.7,15]);
        }}
        
{ // frame #3
translate([0,200,0]){
        translate([0,-15,0])
            cube([2.7,15,150]);
            translate([212.3,-15,0])
            cube([2.7,15,210]);
            translate([312.3,-15,0])
            cube([2.7,15,230]);
        translate([0,0,0])
            cube([15,2.7,150]);
        translate([100,0,0])
            cube([15,2.7,180]);
        translate([200,0,0])
            cube([15,2.7,210]);
        translate([300,0,0])
            cube([15,2.7,240]);
        translate([0,-2.7,60])
            cube([215,2.7,15]);
        translate([200,-2.7,20])
            cube([115,2.7,15]);
        translate([-50,-2.7,120])
            rotate([0,-15,0])
            cube([400,2.7,15]);
        }
}
        
{ // matress
    color("white")
    translate([15,10,77.7])
        cube([140,190,10]);
        } 
        
{ // east wall
module walle() {
    translate([315,0,30])
    cube([2.7,202.7,15]);   
        }
    for (ez=[0:15:90]) {
        color("orange")
        translate([0,0,ez])
        walle();
        } 
}
{ // interior wall
module walli() {
    translate([215.3,0,60])
    cube([2.7,202.7,15]);   
        }
    for (z=[0:-15:-40]) {
        color("red")
        translate([0,0,z])
        walli();
        } 
}
{ // bed floor
module floorb() {
    translate([15,0,75])
    cube([15,202.7,2.7]);   
        }
    for (x=[0:15:190]) {
        color("maroon")
        translate([x,0,0])
        floorb();
        } 
}