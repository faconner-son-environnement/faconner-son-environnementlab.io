pL = 7.5;
pl = 2.5;
module arba()  { // arbalete
color("orange")
    {
    rotate([-20,0,0])
        cube([pl,pL,200]);
    translate([pl,140,0])
    mirror([0,90,0])
    rotate([-20,0,0])    
    cube([pl,pL,200]);
    }
}
{ // arbaletes
     for (a =[0:3])
     {
     translate([a*(100+(2*pl)),0,0])    
     arba();
     }
} 
{ // planchers
     for (a =[0:2])
     {
     translate([a*(100+(2*pl)),0,0])    
     plan();

     }
} 

module plan()  {//plancher
    color("lightblue")
    translate([2*pl,10,30])
       { 
    translate([pl,0,0])
    cube([100-2*pl,pl,pL]);
      translate([pl,120-pl,0])
        cube([100-2*pl,pl,pL]);
        cube([pl,120,pL]);
        translate([100-pl,0,0])
        cube([pl,120,pL]);
                   translate([100,12,0])
        cube([pl,108,pL]);
       translate([-pl,0,0])
       cube([pl,108,pL]);
       }
}
/*{ // rdc
color("yellow",0.6)
translate([0,0,hsp+s])
    {
        //mur_sud
    translate([l-mp,0,0])
    difference(){
        cube([2,L,hsp]);  
        translate([-1,18,0])
        cube([4,14,22]);
        translate([-1,48,0])
        cube([4,44,22]);
        }
        translate([l/2,36,0])
    cube([2,75,hsp]);
    translate([0,36,0])
        cube([l,2,hsp]);
               //mur_nord
    translate([0,0,0])
    difference(){
        cube([2,L,hsp]);  
        translate([-1,18,0])
        cube([4,14,22]);
        translate([-1,68,0])
        cube([4,10,22]);
        } 
        //dalle
    translate([0,0,hsp])   
       cube([l,L,2]);
    }
}
//{ // 1er
color("orange",0.8)
translate([0,0,2*(hsp+s)])
    {
    translate([39,0,0])
    cube([1,120,25]);
    }
}
//{ // roof
color("darkgrey",1)
translate([l/2,0,2*(hsp+s)])
    {
translate([-45,0,0])
rotate([0,40,0])    
   cube([2,L,70]);  
    mirror([180,0,0])
    {
    translate([-45,0,0])
    rotate([0,40,0])    
   cube([2,L,70]);  
    }
}
}
//{
    color("blue",0.3)
translate([l,40,15])
cube([20,L-40,60]);  
    }
//{//grass
color("green",1)
    {
translate([100,0,15])
    cube([100,L,1]);
translate([-100,80,0])
    cube([100,35,1]);
rotate([0,-10,0])        
translate([-100,0,17])
    cube([100,80,1]);
    }  
}
//{//deck
color("brown",1)
translate([0,0,hsp+2.5])
{
translate([-10,0,0])
    cube([l+30,L+20,1]);
}
}*/