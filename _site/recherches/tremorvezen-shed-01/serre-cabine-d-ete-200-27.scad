
{ // cadres
module cadre() {
    //poteau#1
translate([0,5,0])
            color("orange")

    cube([20,2.5,151]);
translate([0,0,0])
    cube([20,2.5,151]);
    //poteau#2
translate([180,5,0])
    cube([20,2.5,217]);
translate([180,0,0])
    cube([20,2.5,217]);    
    //poteau#3
translate([360,0,0])
    cube([20,2.5,290]);
translate([360,5,0])
    cube([20,2.5,290]);
//poutrehaute
translate([0,2.5,60])
    cube([200,2.5,20]);
translate([0,7.5,60])
    cube([200,2.5,20]);
//poutrebasse
translate([180,2.5,20])
    cube([200,2.5,20]);
translate([180,7.5,20])
    cube([200,2.5,20]);    
//solive    
translate([7,2.5,132])
rotate([0,-20,0])
    cube([400,2.5,20]);
        }
    for (y=[0:100:300]) {
        color("lightgreen")
        translate([0,y,0])
        cadre();
        } 
}   
     
{ // matress
    color("white")
    translate([20,5,82.5])
        cube([140,200,10]);
        } 
        
{ // east wall
module walle() {
    translate([380,0,140])
    cube([2.5,307.5,20]);   
        }
    for (z=[0:-20:-120]) {
        color("orange")
        translate([0,0,z])
        walle();
        } 
}
{ // interior wall
module walli() {
    translate([200,0,60])
    cube([2.5,205,20]);   
        }
    for (z=[0:-20:-20]) {
        color("lightblue")
        translate([0,0,z])
        walli();
        } 
}
{ // floor
module floor() {
    translate([200,0,40])
    cube([20,305,2.5]);   
        }
    for (x=[0:20:160]) {
        color("lightblue")
        translate([x,0,0])
        floor();
        } 
}
{ // bed floor
module floorb() {
    translate([182.5,0,80])
    cube([20,205,2.5]);   
        }
    for (x=[0:-20:-180]) {
        color("maroon")
        translate([x,0,0])
        floorb();
        } 
}

{//meuble_cuisine
translate([320,0,40])
    cube([60,307.5,90]);   
    }
{//man
 translate([260,30,40])
    cube([60,20,183]);   
    }