
{ // Deck  
    { // DeckFrame
translate([2.5,0,0])
cube([195,2.5,10]);
translate([0,60,0])
    cube([200,2.5,10]);
translate([0,120,0])
    cube([200,2.5,10]);
translate([0,180,0])
    cube([200,2.5,10]);
color("red")
cube([2.5,180,10]);
translate([197.5,0,0])
    cube([2.5,180,10]);
    }
    { // DeckFloor
    module board() {
        translate([0,0,10])
            cube([10,180,2.5]);
        }
    for (dx=[0:10:195]) {
        color("tan")
        translate([dx,0,0])
        board();
        }
    }
}
{ // wall01
    translate([0,0,12.5])
    {
cube([200,10,2.5]);
translate([0,0,2.5])
    cube([2.5,10,97.5]);
translate([197.5,0,2.5])
    cube([2.5,10,97.5]);
translate([0,0,100])
    cube([200,10,2.5]);
    }
}
{
 color("red")
 translate([0,0,115])
    {
    rotate([45,0,0])
        cube([2.5,180,10]); 
    translate([0,180,0])
        rotate([135,0,0])
        cube([2.5,180,10]);
       
    }  
}