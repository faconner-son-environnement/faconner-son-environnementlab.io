---
layout: layout.liquid
title: Ressources mobilier à trier
tags:
  - ressource
  - mobilier
---
Ressources mobilier à trier
=======

<mark>à documenter</mark>

+ [Table et Chaises de David Enon](http://david.enon.free.fr/vrac/TABLES%20et%20CHAISE/)
+ [Tréteaux de David Enon](http://david.enon.free.fr/vrac/TRETEAUX/)
+ [Tréteaux d'Aurélien Barbry](https://www.aa13.fr/design-dobjet/trestles-aurelien-barbry-6230)
+ [workshop with Le Van Bo, “Build your own Berliner Hocker.”](http://www.berlinartlink.com/2011/03/18/betahus-baustelbar-2/)
+ [libreobjet.org/](http://libreobjet.org/)
+ [yi-con-lu](https://crapisgood.com/yi-con-lu/)
+ [ro-lu](https://crapisgood.com/a-rolu-reader/)
+ [stiff.design](http://stiff.design/pages/309-projekte)
+ [Narrative Machine](https://crapisgood.com/the-narrative-machine-at-buro-bdp/)

![Assise en porte-bouteilles](assise-porte-bouteilles-001.png)
