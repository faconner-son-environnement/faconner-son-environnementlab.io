---
layout: layout.liquid
title: Le projet Osthang
tags:
  - ressource
  - bati
  - abri
---

Le projet Osthang
=======

Le projet [osthang](http://raumlabor.net/osthang-project/), orchestré par Raumlabor, a eu lieu en été 2014 sur le terrain sous-utilisé de l'Osthang, situé au milieu de l'ensemble historiquement significatif de l'ancienne colonie d'artistes Mathildenhöhe. En tant qu'école d'été et festival, il a rassemblé des connaissances et des expériences en matière d'architecture, de sciences sociales et politiques, d'économie, d'activisme et d'art, ainsi que de construction et de vie expérimentale.</p>

![Osthang Main Hall](osthang-main-hall-001.png)

Pour se projet, ConstructLab a travaillé sur [un micro village](http://www.constructlab.net/projects/constructlab-builders-village/) pour les personnes qui dormaient sur le site dès le début des travaux.

![Osthang Builders Village](osthang-builders-village-001.png)

## Différentes ressources du projet

[le projet documenté par collectif etc](http://www.collectifetc.com/realisation/osthang-project/)</p>

![Osthang Kitchen](osthang-kitchen-001.png)
