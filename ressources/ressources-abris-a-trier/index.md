---
layout: layout.liquid
title: Ressources abris à trier
tags:
  - ressource
  - bati
  - abri
---
Ressources abris à trier
=======

<mark>à documenter</mark>

+ Le [jardin des remparts](https://bruitdufrigo.com/projets/fiche/jardin-des-remparts/)
+ Le [Hotel Shabbyshabby](https://raumlabor.net/hotel-shabbyshabby/)
+ [Observation house](https://dylangauthier.info/observationhouse)
+ [Guest Studio](https://www.atlasofplaces.com/architecture/guest-studio/) - Glenn Murcutt
+ [Aldea - First steps of inhabitation](http://www.constructlab.net/projects/aldea/) - ConstructLab
+ [Community Kitchen of Terras da Costa - Atelier Mob](https://www.archdaily.com/775891/community-kitchen-of-terras-da-costa-ateliermob-plus-projecto-warehouse?ad_medium=gallery)
+ [Chinoiserie](http://encoreheureux.org/projets/chinoiserie/) - Encore Heureux
+ [Cabanon Cuyes](https://bruitdufrigo.com/projets/fiche/cabanon-cuyes/) - Bruit du frigo
+ [Cass Studio](https://practicearchitecture.co.uk/project/cass-studio/) - Practice Architecture
+ [Bothy Project](https://www.bothyproject.com/) - Iain MacLeod and Bobby Niven
+ [Bermondsey Bothy](https://southwarkparkgalleries.org/launch-bermondsey-bothy/)
+ [Climax](https://collectifparenthese.com/project/show/23) - Collectif Parenthèse
+ [Micro Dwellings](https://www.n55.dk/MANUALS/MICRO_DWELLINGS/micro_dwellings.html) - n55
+ [humanitarianlibrary.org/shelter-and-settlement](https://www.humanitarianlibrary.org/shelter-and-settlement)
+ [Greenhouse Hotel](https://popupcity.net/observations/greenhouse-hotel-shows-a-glimpse-of-the-future-of-food-production-landscapes/) - [hihahut.nl](https://hihahut.nl/)
+ [Aire de repos](https://atelier-craft.com/airederepos) de l'atelier Craft
+ [Estacione Spacial Arquitectos](https://estacionespacialarquitectos.wordpress.com/) - qui travaillent souvent avec des *A Frames*
+ [the quality of not being available when needed](https://divisare.com/projects/325889-gartnerfuglen-arkitekter-noun-1-unavailability-the-quality-of-not-being-available-when-needed)
+ [escuela nueva esperanza](https://www.metalocus.es/es/noticias/escuela-nueva-esperanza)
+ [archdaily.com/tag/tu-berlin](https://www.archdaily.com/tag/tu-berlin)
+ [Sauna](https://www.dwell.com/article/off-grid-sauna-studio-rain-10fc004b)
+ [Gallery of Solar Greenhouse Prototype / IAAC](https://iaac.net/maebb-project-solar-greenhouse/)
+ [The Hermitage - Llabb](https://llabb.eu/progetti/the-hermitage)- 
