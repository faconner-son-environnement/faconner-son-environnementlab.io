---
layout: layout.liquid
title: Collectifs à suivre
tags:
  - ressource
  - bati
  - autre
  - lowtech
---
Collectifs à suivre
=======

+ [Raumlabor Berlin](https://raumlabor.net/)
+ [Bruit du frigo](https://bruitdufrigo.com/)
+ [Collectif etc](http://www.collectifetc.com/)
+ [Construct Lab](http://www.constructlab.net/)
+ [Encore Heureux](http://encoreheureux.org/)
+ [Collectif Baya](https://collectifbaya.com/)
+ [Collectif Parenthèse](https://collectifparenthese.com/)
