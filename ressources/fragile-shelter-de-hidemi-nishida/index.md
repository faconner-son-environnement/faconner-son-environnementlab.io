---
layout: layout.liquid
title: Fragile Shelter de Hidemi Nishida
tags:
  - ressource
  - abri
  - bati
---
Fragile Shelter de Hidemi Nishida
=======

![Photo du Fragile Shelter](fragile-shelter-001.png)

![Photo du Fragile Shelter](fragile-shelter-002.png)

![Photo du Fragile Shelter](fragile-shelter-003.png)

&rarr; Retrouver le projet [Fragile Shelter](http://www.hdmnsd.com/works-spatial-works/fragile-shelter/) sur le site de Hidemi Nishida.
