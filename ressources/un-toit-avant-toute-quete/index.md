---
layout: layout.liquid
title: Un toit avant toute quête
tags:
  - ressource
  - bati
  - abri
---

Un toit avant toute quête
=======

Foi de survivaliste, la fabrication d'un abri passe avant la quête d'eau et de nourriture.</p>

## Inspirations

+ Le projet [Osthang](/ressources/le-projet-osthang/), orchestré par Raumlabor.
+ Le projet [Fragile Shelter](/ressources/fragile-shelter-de-hidemi-nishida/) sur le site de Hidemi Nishida

## Recherches/Projets

Peut-on penser une démarche équivalente au projet <em>Autoprogettazione</em> d'Enzo Mari non plus pour du mobilier mais pour du bâti? Comment mettre au point une technique constructive rudimentaire et des matériaux courants?

+ taille du bâti
+ vie int. et ext.
+ meubles à habiter
