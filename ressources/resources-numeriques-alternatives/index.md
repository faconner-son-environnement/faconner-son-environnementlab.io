---
layout: layout.liquid
title: Ressources numériques alternatives
tags:
  - ressource
  - outil
  - lowtech
  - numerique
---

Ressources numériques alternatives
=======

Ressources à trier

+ [meshtastic.org/](https://meshtastic.org/)
+ [Briar](https://f-droid.org/fr/packages/org.briarproject.briar.android/)
