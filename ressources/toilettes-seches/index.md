---
layout: layout.liquid
title: Toilettes sèches
tags:
  - ressource
  - eau
  - autre
  - lowtech
---
Toilettes sèches
=======

![Toilettes en plein air](/images/loo-shed-001.gif)

Il existe de nombreux systèmes de toilettes sèches. Ici, le modèle proposé est dit à litière biomaitrisée TLB. C'est le plus simple des modèles, qui ne nécessite aucune ventilation. Ce modèle est constitué d'un seau en inox qui reçoit les déjections (urine et excrément), le papier toilette ainsi que de la matière végétale carbonée. Que ce soit dans la pièce où sont installées les toilettes, que dans la zone de compostage, très peu d'odeurs sont émises. (En fait pas plus que dans des toilettes à eau.) Voir un [plan de montage](https://wiki.lowtechlab.org/wiki/Toilettes_s%C3%A8ches_familiales) sur le wiki du LowTechLab.

&rarr; Voir le projet de [toilettes sèches transportables](/projets/toilettes-seches-transportables/) mené à Kerminy.
