---
layout: layout.liquid
title: Le manifeste Autoprogettazione d'Enzo Mari
tags:
  - ressource
  - mobilier
---
Le manifeste Autoprogettazione d'Enzo Mari
=======

Une référence incontournable : le projet Autoprogettazione d'Enzo Mari.

> [...] véritable manifeste visant à révolutionner le monde de la distribution. Il proposait de donner aux particuliers un accès direct aux plans constructifs d’une série de meubles facilement réalisables par tout un chacun, à l’aide de planches standard et de matériel de bricolage usuel (marteau, scie, clous et colle). Ces plans, distribués gratuitement pendant l’exposition, furent ensuite réunis sous forme de livre.

Voir un [article sur IndexGrafik](http://indexgrafik.fr/autoprogettazione-enzo-mari/).

![maquettes du mobilier autoprogettazione d'Enzo Mari](autoprogettazione-enzo-mari-001.png)

Nombreux ont construit du mobilier en suivant les instructions d'Enzo Mari. Le Conseil d'Architecture d'Urbanisme et de l'Environnement des Côtes d'Armor a éditer un [fascicule pour construire une table et des chaises Enzo Mari](https://lieu-subjectif.com/documents/caue-22-rietveld-mari.pdf) (+ fauteuil Rietvield). Voir aussi [Autoprogettazione Revisited](https://lieu-subjectif.com/documents/autoprogettazione-revisited.pdf)
~~Autre exemple avec [Dylan Gauthier](https://dylangauthier.info/Various-Display-Structures-for-Enzo-Mari)~~.

<mark>ToDo: Scanner le bouquin d'Enzo Mari</mark>


## Voir aussi

+ [La chaise bordelaise de Raumlabor](/ressources/la-chaise-bordelaise-de-raumlabor)
+ [Lucas Maassen](http://www.lucasmaassen.com/)
+ [Keung Caputo](https://www.kueng-caputo.ch/projects/)
+ [Graham Hudson](http://grahamhudson.com/)
+ [Joe Pipal](https://pipal.co.uk/)
+ [Andrea de Chirico](http://andreadechirico.com/)
