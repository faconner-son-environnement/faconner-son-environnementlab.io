---
layout: layout.liquid
title: Build A Yurt — Un projet de Kueng Caputo
tags:
  - ressource
  - bati
  - abri
---
Build A Yurt — Un projet de Kueng Caputo
=======

![Build A Yurt](kueng-caputo-build-a-yurt-001.png)

+ [Instructions Build A Yurt](https://lieu-subjectif.com/documents/kueng-caputon-build-a-yurt.pdf)
