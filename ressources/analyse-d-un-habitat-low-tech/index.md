---
layout: layout.liquid
title: Analyse d'un habitat low-tech
tags:
  - ressource
  - bati
  - autre
  - lowtech
---
Analyse d'un habitat low-tech
=======

En 2019, Clément Chabot et Pierre-Alain Lévêque, du Low-Tech Lab, entreprennent la construction d’un micro-habitat démonstrateur, qu’ils équipent des 12 low-tech documentées. Afin de tester la pertinence de chacune des low-tech au fil des saisons, ils y ont vécu pendant un an et ont évalué les systèmes sous différents aspects :

+ l’impact environnemental, en mesurant l’impact carbone grâce à l’Analyse de leur Cycle de Vie (ACV)
+ l’intérêt financier et économique en comparaison à d’autres solutions conventionnelles
+ l’ergonomie des systèmes, et notamment l’utilité, la fonctionnalité, le confort, l’efficacité et la compatibilité avec nos modes de vies

Tout au long de l’expérimentation, Clément et Pierre-Alain ont relaté leurs expériences dans une web-série disponible sur Youtube et sur [le site du Low-Tech Lab](https://lowtechlab.org/fr/le-low-tech-lab/les-actions/habitat-low-tech). En février 2020, un [rapport de cette expérimentation](https://lieu-subjectif.com/documents/rapport-experimentation-habitat-low-tech-low-tech-lab.pdf) est disponible.

![image extérieure de l'habitat low-tech du low-tech lab](low-tech-lab-habitat-ext-001.png)
![image intérieure de l'habitat low-tech du low-tech lab](low-tech-lab-habitat-int-001.png)
