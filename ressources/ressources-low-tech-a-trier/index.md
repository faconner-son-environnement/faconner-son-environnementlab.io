---
layout: layout.liquid
title: Ressources low-tech à trier  
tags:
  - ressource
  - outil
  - lowtech
---

Ressources low-tech à trier
=======

Ressources à trier

+ [Slow Tools Fast Change](https://www.stonebarnscenter.org/blog/slow-tools-fast-change/)
+ [Fours solaires](https://lytefire.com/diy)
+ [Fours solaires](https://solar-fire-france.fr/)
+ [david.mercereau.info/](https://david.mercereau.info/)
+ [Bolivia Inti](https://www.boliviainti-sudsoleil.org/)
+ [Poêle pour habitat léger](http://www.re-cycle-age.com/SiteNNV3/?page_id=1421)
+ [howtopedia.org/](http://en.howtopedia.org/)
+ [appropedia.org/](https://www.appropedia.org/Welcome_to_Appropedia)
