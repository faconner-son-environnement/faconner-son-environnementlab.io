---
layout: layout.liquid
title: La chaise bordelaise de Raumlabor
tags:
  - ressource
  - mobilier
---
La chaise bordelaise de Raumlabor
=======

<mark>ToDo: à documenter</mark>

La chaise bordelaise est une sculpture participative, éducative et sociale à usage privé pour l'exposition "INSIDERS" à Arc en Reve au CAPC de Bordeaux.

+ [La chaise bordelaise de RaumLabor](http://www.raumlabor.net/?p=2017)

![Image de l'exposition de la chaise bordelaise par Raumlabor](chaise-bordelaise-raumlabor-001.png)
