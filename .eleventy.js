module.exports = {
  templateFormats: [
    "html",
    "md",
    "css",
    "gif",
    "png",
    "jpg",
    "scad"
  ],
  passthroughFileCopy: true
};
