---
layout: layout.liquid
title: Hot Hut
tags:
  - projet
---

Hot Hut
=======

Quelques notes sur cette expérimentation "chauffage" pour passer l'hiver dans [la Hutte de Kerminy](/projets/les-huttes-du-chateau-de-kerminy/).

![dimensions à définir pour cette solution de chauffage par convection naturelle](hot-hut-3D-dimensions-001.png)

@Robin, je me demande si on ne peut pas penser à un rocket double combustion. Ci-dessous un dimensionnement qui permettrait de noyer notre conduit à convection au cœur des 2 chambres de combustion.

![dimensions à définir pour cette solution de chauffage par convection naturelle](hot-hut-vue-haut-dimensions-001.png)

@Robin, @Jeff, est-ce qui ne serait pas bien pour notre conduit à convection de passer d'un diamètre de 80mm à un diamètre de 50mm pour gagner en efficacité. On devrait gagner en vitesse, non?
