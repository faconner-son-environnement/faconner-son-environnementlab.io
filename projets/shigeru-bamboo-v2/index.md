---
layout: layout.liquid
title: Shigeru Bamboo v2
tags:
  - projet
---

Shigeru Bamboo v2
=======

<mark>En cours</mark>

Une nouvelle version pour Kerminy ? Les connecteurs seraient inspirés de ceux de Shigeru Ban en contreplaqué - dessins et photos ci-dessous.

![Shigeru ban inspi](/projets/shigeru-bamboo-v2/shigeru-inspi-001.png)
![Shigeru ban inspi](/projets/shigeru-bamboo-v2/shigeru-inspi-002.png)

+ Voir des ressources de construction en Bambou sur [humanitarianlibrary.org/](https://www.humanitarianlibrary.org/search-resources?keyword=bamboo)
