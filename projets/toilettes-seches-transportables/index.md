---
layout: layout.liquid
title: Toilettes sèches transportables
tags:
  - projet
---

Toilettes sèches transportables
=======

Pour plus de commodités près du [prototype de hutte](/projets/les-huttes-du-chateau-de-kerminy/), quelques heures auront suffit pour réaliser ces toilettes sèches transportables.

![Plan toilettes sèches transportables](/projets/toilettes-seches-transportables/toilettes-seches-transportables-plan-001.png)
![Toilettes sèches transportables](/projets/toilettes-seches-transportables/toilettes-seches-transportables-001.png)
![Toilettes sèches transportables](/projets/toilettes-seches-transportables/toilettes-seches-transportables-002.png)

## Un peu d'intimité

<mark>En conception</mark>

![inspirations pour la construction d'un bâti](/projets/toilettes-seches-transportables/inspi-bamboo-001.png)
