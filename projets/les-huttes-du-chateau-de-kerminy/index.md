---
layout: layout.liquid
title: Les huttes du château de Kerminy
tags:
  - projet
---

Les huttes du château de Kerminy
=======

Petit histoire de la construction de petits bâtis démontables pour héberger les bénévoles et volontaires qui viennent apprendre et aider sur la ferme du château en été. La documentation (à compléter) est sur le site de Cyclo-farm: [cyclo-farm.kerminy.org/les_huttes](https://cyclo-farm.kerminy.org/bivouak).

## 20 juillet - Pyc emménage

Le prototype est terminé (même s'il y aura encore pas mal d'ajustements). Il est habitable et habité. Vous pouvez me demander à venir tester ce logement à coût zero.
![Vue extérieure](/projets/les-huttes-du-chateau-de-kerminy/hutte-kerminy-proto-ok-exterieur-001.png)
![Vue de l'intérieure](/projets/les-huttes-du-chateau-de-kerminy/hutte-kerminy-proto-ok-interieur-001.png)
![Constructeur fier de son œuvre](/projets/les-huttes-du-chateau-de-kerminy/hutte-kerminy-proto-ok-pyc-001.png)

## 1er juillet 2021 - présentation au Low-Tech Lab

Le prototype des huttes prend enfin l'air (et la pluie) pour une présentation au [Low-Tech Lab](https://lowtechlab.org/fr) de passage à Kerminy.
![Le prototype de hutte en extérieur](/projets/les-huttes-du-chateau-de-kerminy/hutte-kerminy-proto-exterieur-001.png)

## Fin mai 2021, un premier prototype

Quelques jours de travail en compagnie de Dom, Erika et Hugo pour construire ce premier prototype de hutte au château de Kerminy. Le concept avait été élaboré en amont par Dom et Jean-François.

Ce prototype a été réalisée avec du bois de palettes récupérés, un rouleau pare-pluie, des canisses en roseaux trouvées sur le bon coin, des vis et des clous, des tiges filetées et des parpaings pleins pour les plots.

![Façade du prototype de la hutte](hutte-kerminy-proto-facade-001.jpg)
![Test de la couverture en roseaux du prototype de la hutte](hutte-kerminy-proto-roseaux-001.jpg)

### Des poutres en I

Pour supporter la terrasse qui va accueillir la hutte, nous avons conçus un modèle de poutres en I de 4m de long à partir de bois de palettes de 110cmX8cmX2,5cm. Ces poutres, pour ne pas être trop souples, doivent être supportées tous les 2 mètres.

![montage de poutre en I](hutte-kerminy-proto-montage-poutres-001.png)
![montage de poutre en I](hutte-kerminy-proto-montage-poutres-002.png)
![montage de poutre en I](hutte-kerminy-proto-montage-poutres-003.png)
