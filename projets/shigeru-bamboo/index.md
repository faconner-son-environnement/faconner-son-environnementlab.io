---
layout: layout.liquid
title: Shigeru Bamboo
tags:
  - projet
---

Shigeru Bamboo
=======

![Shigeru Bamboo](/projets/shigeru-bamboo/shigeru-enzo.png)

<mark>En cours</mark>

Travail sur l'architecture d'urgence de Shigeru Ban - Prototype en bambou. Éte 2020 — Stang Tremorvezen - Nevez

![Shigeru Bamboo](/projets/shigeru-bamboo/shigeru-bamboo-detail-001.png)

![Shigeru Bamboo](/projets/shigeru-bamboo/shigeru-bamboo-detail-002.png)

![Shigeru Bamboo](/projets/shigeru-bamboo/shigeru-bamboo-detail-003.png)

![Shigeru Bamboo](/projets/shigeru-bamboo/shigeru-bamboo-general-001.png)

![Shigeru Bamboo](/projets/shigeru-bamboo/shigeru-bamboo-general-002.png)

![Shigeru Bamboo](/projets/shigeru-bamboo/shigeru-bamboo-notes-001.png)
