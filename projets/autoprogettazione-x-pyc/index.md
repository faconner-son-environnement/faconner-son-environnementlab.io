---
layout: layout.liquid
title: Autoprogettazione x Pyc
tags:
  - projet
---

Autoprogettazione x Pyc
=======

## Nouvelles tables pour Kerminy?

![inspiration](/projets/autoprogettazione-x-pyc/enzo-inspi-001.png)
![inspiration](/projets/autoprogettazione-x-pyc/enzo-inspi-002.png)
![inspiration](/projets/autoprogettazione-x-pyc/enzo-inspi-003.png)
![inspiration](/projets/autoprogettazione-x-pyc/enzo-inspi-004.png)

## Éte 2020 — Stang Tremorvezen
Planches de chantier en 200mm x 27mm de large — le piétement est découpé dans ces mêmes planches 50mm x 27mm.

![Autoprogettazione X Pyc 03 ](/projets/autoprogettazione-x-pyc/autoprogettazione-pyc-03-01.png)
![Autoprogettazione X Pyc 03 ](/projets/autoprogettazione-x-pyc/autoprogettazione-pyc-03-notes-01.png)

## Éte 2013 - Le Haut Cornille - Trôo
Planches de chantier en 100mm x 27mm de large.

![Autoprogettazione X Pyc 02 ](/projets/autoprogettazione-x-pyc/autoprogettazione-pyc-02-01.png)

## 2010 ??? - Le Haut Cornille - Trôo
Planches de chantier en 200mm x 27mm de large et tasseau pour piétement.

![Autoprogettazione X Pyc 01 ](/projets/autoprogettazione-x-pyc/autoprogettazione-pyc-01-01.png)
![Autoprogettazione X Pyc 01 ](/projets/autoprogettazione-x-pyc/autoprogettazione-pyc-01-02.png)
