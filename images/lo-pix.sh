#!/bin/sh — sh lo-pix.sh nom-et-chemin-du-fichier.jpg (j'ai la mémoire qui flanche!)

convert $1 -resize 640x640 -sharpen 0x2.0 -dither FloydSteinberg -colors 16 ${1%.*}.png
