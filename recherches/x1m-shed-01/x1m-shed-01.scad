//Taille de la planche de palette type
L = 100;
l = 10;
ep = 2;
ll = 140;

module halfFrame() {
translate([0,0,0])
    cube([l,ep,L]);
translate([0,2*ep,0])
    cube([l,ep,L]);
        
translate([-l,ep,L-l])
rotate([0,45,0])
    cube([l,ep,L]); 
    }
module fullFrame() {
    translate([ll,0,0])
mirror([45,0,0])
    halfFrame();  
    halfFrame();
translate([0,ep,l])
    cube([ll,ep,l]);
  translate([55,0,145])
    cube([30,ep,l]);   
    }     
{ // frames

     for (a =[0:2])
     {
     translate([0,a*100,0])    
     fullFrame();
     }
}

{//couchage 200*140
color("lightblue")

translate([0,0,20])
    cube([ll,216,1]);       
        
        
    }
    /*
    */