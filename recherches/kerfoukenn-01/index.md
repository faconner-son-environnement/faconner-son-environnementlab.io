---
layout: layout.liquid
title: Kerfoukenn 01
tags:
  - recherche
---

Kerfoukenn 01
=======

<mark>En cours</mark>

![Kerfoukenn](/recherches/kerfoukenn-01/kerfoukenn-01-maquette-001.png)
maquette (w/ perso 183cm) - attention la Kerfoukenn 01 est maintenant moins haute et plus large pour faciliter l'assise.

![Kerfoukenn](/recherches/kerfoukenn-01/kerfoukenn-01-general-001.png)
Vue générale

![Kerfoukenn](/recherches/kerfoukenn-01/kerfoukenn-01-arbalete-plancher-001.png)
détail assemblage arbalètriers et planchers

![Kerfoukenn](/recherches/kerfoukenn-01/kerfoukenn-01-idee-couverture-001.png)
![Kerfoukenn](/recherches/kerfoukenn-01/kerfoukenn-01-inspi-couverture-001.png)
idée couverture inspiré des toits-bâches des marchés

![Kerfoukenn](/recherches/kerfoukenn-01/kerfoukenn-01-detail-couverture-001.png)
détail de la couverture pare-pluie




Le [fichier .scad](/recherches/kerfoukenn-01/kerfoukenn-01.scad) est disponible (clic-droit "enregistrer la cible...").
