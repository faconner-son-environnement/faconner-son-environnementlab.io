---
layout: layout.liquid
title: Tremorvezen Shed
tags:
  - recherche
---

Tremorvezen Shed
=======

<mark>Recherches en cours</mark>

![Tremorvezen Shed 01](tremorvezen-shed-001.png)
![Tremorvezen Shed 01](tremorvezen-shed-002.png)
![Tremorvezen Shed 02](tremorvezen-shed-003.png)
![Détail Tremorvezen Shed 02](tremorvezen-shed-004.png)

Les fichiers .scad sont disponibles ci-dessous (clic-droit "enregistrer la cible..."):

+ [tremorvezen-shed-01](/recherches/tremorvezen-shed/tremorvezen-shed-01.scad)
+ [tremorvezen-shed-02](/recherches/tremorvezen-shed/tremorvezen-shed-02.scad)
