---
layout: layout.liquid
title: Accueil
tags:
  - accueil
---

Façonner son environnement immédiat
=======

> Une autre possibilité s’offre. La science peut aussi s’employer à simplifier l’outillage, à rendre chacun capable de façonner son environnement immédiat, c’est-à-dire capable de se charger de sens en chargeant le monde de signes

— La convivialité - Ivan Illich

---

<section id="ress">

<section>
<h2>Actu</h2>
Le <a href="/projets/les-huttes-du-chateau-de-kerminy/">prototype des huttes</a> est terminé (même s'il y aura encore pas mal d'ajustements). Il est habitable et habité. Vous pouvez demander à venir tester ce logement à coût zero.
<img src="/projets/les-huttes-du-chateau-de-kerminy/hutte-kerminy-proto-ok-exterieur-001.png" alt="Le prototype de hutte terminé">
</section>

<section>
<h2>Abris</h2>
<ul>
{%- for post in collections.bati -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul></section>

<section>
<h2>Mobilier</h2>
<ul>
{%- for post in collections.mobilier -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
</section>

<section>
<h2>low-tech</h2>
<ul>
{%- for post in collections.lowtech -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
</section>

<section>
<h2>art</h2>
<ul>
{%- for post in collections.art -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
</section>


<section>
<h2>autres</h2>
<ul>
{%- for post in collections.autre -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
</section>

<figure>
<img class="noborder" src="/images/angele-veut-une-cabane-001.png" alt="Angèle veut une cabane">
<figcaption>Dessin Angèle Carnoy 2021</figcaption>
</figure>

<section>
<h2>projets</h2>
<ul>
{%- for post in collections.projet -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
</section>

<section>
<h2>recherches</h2>
<ul>
{%- for post in collections.recherche -%}
<li><a href="{{ post.url }}">{{ post.data.title }}</a></li>
{%- endfor -%}
</ul>
</section>

</section>


--------
## Liens à documenter et à classer

+ [OpenStructures](https://openstructures.net/)
+ [Earthship](https://www.earthshipglobal.com/) - Voir aussi le reportage de Nomade des mers sur Arte
+ [outofthevalley.co.uk/](https://outofthevalley.co.uk/)
+ [Tiny house Cahute](https://cahute.com/)
+ [folke köbberling & martin kaltwasser](http://www.koebberlingkaltwasser.de/works.html)
+ [Oasis de la maison autonome](http://heol2.org/)
+ [ernesto oroza](https://www.ernestooroza.com/) — Artiste inspiré par le lowTech Cubain
+ [éclairage LowTech](https://deciwatt.global/)
+ [éolienne](https://www.tripalium.org/manuel)
+ [Rural Studio](http://ruralstudio.org/)
+ [Bâti réversible bruxelles](https://www.guidebatimentdurable.brussels/fr/reversibilite-et-reemploi.html?IDC=10657)
+ The Segal Method
+ [L'observatoire de Kawamata](https://www.estuaire.info/fr/oeuvre/l-observatoire-tadashi-kawamata/)
+ [Micro Architectures](https://www.citedelarchitecture.fr/fr/article/references-de-microarchitectures)
+ Shigeru Ban et l'architecture d'urgence
+ Le [low-cost-design-yes-please](https://www.archistart.net/news/low-cost-design-yes-please/) <mark>à documenter</mark>
+ [Living-Tools by Yi-Cong Lu](https://www.dezeen.com/2011/06/14/living-tools-by-yi-cong-lu/) et [autre ressource](https://www.archdaily.pe/pe/750348/serie-living-tools-yi-cong-lu?ad_medium=gallery)
+ [Four à pain](https://www.baupalast.berlin/workshops/koch-und-backwerkstatt/)
+ [Low-Tech skol](https://kerlotec.com/low-tech-skol/)
+ [Kerlotec](https://kerlotec.com/)
+ [Fédération des Récupérathèques](http://federation.recuperatheque.org/)
+ [wikkelhouse.com/](https://wikkelhouse.com/)
+ [Merril Sinéus](https://merrilsineusarchitecte.tumblr.com/)
+ [U-Build](https://u-build.org/) de Studio Bark
+ [ÜTE](https://www.ute-nanohabitat.fr/)
+ [Mini Living](https://www.dezeen.com/miniliving/)
+ [Andrea Zittel](https://www.zittel.org/)
+ [https://studioacte.com/#/circular-pavillion/](https://studioacte.com/#/circular-pavillion/)
+ [Adhocracy](http://adhocracy.athens.sgt.gr/i)
+ [https://underprospective.com/](https://underprospective.com/)
+ [la Maison Magique](https://arqa.com/en/_arqanews-archivo-en/la-maison-magique.html) - voir aussi sur le [site de Didier Faustino](https://didierfaustino.com/)
